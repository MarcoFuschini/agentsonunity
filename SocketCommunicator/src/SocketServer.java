import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.naming.CompoundName;


public class SocketServer{

	ServerSocket serverSocket;
	Socket connection;
	int port=5111;
	
	PrintWriter out;
	BufferedReader in;
	private boolean active=true;
	
	public SocketServer(int port){
		
		try {
			this.port=port;
			serverSocket=new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void startServer(IPrintable msgBoard){
		Thread t=new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					msgBoard.println("SERVER: attesa connessione");
					connection=serverSocket.accept();
					msgBoard.println("SERVER: connection accepted ->"+connection.toString());
					out=new PrintWriter(connection.getOutputStream());
					in= new BufferedReader(new InputStreamReader(connection.getInputStream()));
					
					//************************************************//
					
					while(active){
						if(in.ready()){
							String msg=read();
							msgBoard.println("SERVER: rivevuto->"+msg);
						}
					}
					
								
				} catch (IOException e) {
					e.printStackTrace();
				}
				msgBoard.println("SERVER: stopping server");
			}
		});
		t.start();
	}
	
	public String read(){
		try {
			return in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	public void write(String msg){
		out.println(msg);
		out.flush();
	}
	
	public void StopServer(){
		active=false;
	}
	
}
