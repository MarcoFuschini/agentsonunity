import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameListener implements ActionListener {

	
	MyFrame parent;
	
	public FrameListener(MyFrame parent) {
		this.parent=parent;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd=e.getActionCommand();
		
		if("Connect".equals(cmd)){
			parent.clientConnection(parent.getFrameView().getAdd(), parent.getFrameView().getPort());
		}
		if("Disconnect".equals(cmd)){
			parent.disconnect();
		}
		if("Send".equals(cmd)){
			parent.send(parent.getFrameView().getMsg());
		}
		if("Listen".equals(cmd)){
			parent.listen(parent.getFrameView().getPort());
		}
	}

}
