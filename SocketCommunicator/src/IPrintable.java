
public interface IPrintable {

	public void println(String msg);
}
