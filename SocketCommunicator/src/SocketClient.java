import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketClient {

	Socket socket;
	PrintWriter out;
	BufferedReader in;
	IPrintable msgBoard;
	String host;
	int port;
	
	public SocketClient(String host,int port){
		this.host=host;
		this.port=port;
		
	}
	
	public void connect(IPrintable msgBoard){
		this.msgBoard=msgBoard;
		try {
			socket=new Socket(host, port);
			msgBoard.println("CLIENT: connection accepted ->"+socket.toString());
			out=new PrintWriter(socket.getOutputStream());
			in= new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void disconnect(){
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void write(String s){
		out.println(s);
		out.flush();
	}
	
	public String readLock(){
		try {
			msgBoard.println("CLIENT: prova lettura");
			return in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
}
