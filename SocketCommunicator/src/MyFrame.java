
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame implements IPrintable {

	
	private FrameView pan;
	private FrameListener fl=new FrameListener(this);
	private boolean server=false;
	private boolean connected=false;
	private boolean active=true;
	
	public SocketClient cl;
	private SocketServer se;
	
	public MyFrame(){
		pan=new FrameView();
		pan.setBorder(new EmptyBorder(20, 20, 20, -100));
//		this.setSize(700, 700);
		this.getContentPane().add(pan);
		this.pack();
//		this.setResizable(false);
		pan.setActionListener(fl);		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			 @Override
			    public void windowClosing(WindowEvent windowEvent) {
			        active=false;
			        connected=false;
			    }
		});
		
		this.setVisible(true);		
	}

	public FrameView getFrameView(){
		return pan;		
	}
	
	
	public void clientConnection(String add,String port){
		cl=new SocketClient(add, Integer.parseInt(port));
		cl.connect(this);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(active){
					println("CLIENT: provo a leggere");
					String msg=cl.readLock();
					println(msg);
				}
				
			}
		}).start();
		server=false;
		connected=true;
	}

	public void disconnect() {
		if(server){
			se.StopServer();
			connected=false;
			server=false;
		}else{
			cl.disconnect();
			connected=false;
			println("Disconnected");
			cl=null;
		}
	}

	public void send(String msg) {
		if(connected){
			if(server){
				se.write(msg);
				println("Send: "+msg);
			}else{
				cl.write(msg);
				println("Send: "+msg);
			}
		}
	}

	public void listen(String port) {
		se=new SocketServer(Integer.parseInt(port));
		se.startServer(this);
		server=true;
		connected=true;
	}

	public void println(String s) {
		pan.println(s);
	}
	
	

}
