import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JTextArea;

public class CustomOutputStream extends OutputStream {

	private JTextArea txt;
	
	public CustomOutputStream(JTextArea txt) {
		this.txt=txt;
	}

	@Override
	public void write(int b) throws IOException {
		txt.append(String.valueOf((char)b));
//        txt.setCaretPosition(txt.getDocument().getLength());
	}

}
