import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FrameView extends JPanel {

	//COMPONENTI 
	private JLabel addressLbl;
	private JTextField addressTxt;
	private JLabel portLbl;
	private JTextField portTxt;
	
	private JButton connect;//connettere come client
	private JButton disconnect;
	private JButton listen;//per attivare il server
	
	private JTextArea logTxt;
	private JTextField sendTxt;
	private JButton send;
	
	public FrameView() {
		build();
		addressTxt.setText("127.0.0.1");
		/*
		 PrintStream ps=new PrintStream(new CustomOutputStream(logTxt));
		 
		System.setOut(ps);
		System.setErr(ps);
		*/	
	}

	private void build() {
		//creazione effettiva dell'interfaccia
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc=new GridBagConstraints();
		
		//riga0
			//col 0
		addressLbl=new JLabel("Address");
		gbcFormat(gbc, 0, 0, 1, 1);
		this.add(addressLbl, gbc);
			//col 1,2,3
		addressTxt=new JTextField(15);
		gbcFormat(gbc, 1, 0, 3, 1);
		this.add(addressTxt,gbc);
			//col 4
		portLbl=new JLabel("Port");
		gbcFormat(gbc, 4, 0, 1, 1);
		this.add(portLbl,gbc);
			//col5
		portTxt=new JTextField(15);
		gbcFormat(gbc, 5, 0, 3, 1);
		this.add(portTxt,gbc);
		
		//riga 1
			//col 0,1
		connect=new JButton("Connect");
		gbcFormat(gbc, 0, 1, 2, 1);
		this.add(connect,gbc);
			//col 2,3
		disconnect=new JButton("Disconnect");
		gbcFormat(gbc, 2, 1, 2, 1);
		this.add(disconnect, gbc);
			//col 6,7
		listen=new JButton("Listen");
		gbcFormat(gbc, 6, 1, 2, 1);
		gbc.insets=new Insets(5, -30, 0, 0);
		this.add(listen,gbc);
		
		//riga 2,3,4,5
		logTxt=new JTextArea(20, 40);
		gbcFormat(gbc, 0, 2, 8, 4);
		logTxt.setLineWrap(true);
		logTxt.setEditable(false);
		JScrollPane scroll=new JScrollPane(logTxt);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.add(scroll, gbc);
		
		//riga 6
			//col 0,1,2,3,4,5
		sendTxt= new JTextField(35);
		gbcFormat(gbc, 0, 6, 6, 1);
		this.add(sendTxt,gbc);
			//col 6,7
		send = new JButton("Send");
		gbcFormat(gbc, 6, 6, 2, 1);
		this.add(send,gbc);
		
		
	}

	private void gbcFormat(GridBagConstraints gbc,int x,int y, int width, int height){
		gbc.gridx=x;
		gbc.gridy=y;
		gbc.gridwidth=width;
		gbc.gridheight=height;
		gbc.insets=new Insets(5, 0, 0, 10);//padding esterno dei componenti, si applica a tutti se non viene modificato
		gbc.anchor=GridBagConstraints.LINE_START;//allineamento del componente
		//dopo la formattazione � possibile rimodificare i valori
		//cos� non c'� bisogno di risettare il default tutte le volte
	}
	
	public void setActionListener(ActionListener al){
		connect.addActionListener(al);
		disconnect.addActionListener(al);
		listen.addActionListener(al);
		send.addActionListener(al);
	}
	
	public String getAdd(){
		return addressTxt.getText();
	}
	public String getPort(){
		return portTxt.getText();
	}
	public String getMsg(){
		String msg=sendTxt.getText();
		sendTxt.setText("");
		return msg;
	}
	
	public void println(String s){
		logTxt.append(s+'\n');
	}
	

}
