// Agent guardadino in project helloworld

/* Initial beliefs and rules */

/* Initial goals */


/* Plans */

+tick:count(C) <- .print(C).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
{ include("$jacamoJar/templates/org-obedient.asl") }
