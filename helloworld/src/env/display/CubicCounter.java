// CArtAgO artifact code for project helloworld

package display;

import cartago.*;
import jason.stdlib.string;

public class CubicCounter extends Artifact {
	void init(int initialValue) {
		defineObsProperty("count", initialValue);
	}
	
	@OPERATION
	void inc() {
		ObsProperty prop = getObsProperty("count");
		prop.updateValue(prop.intValue()+1);
		signal("tick");
	}
	
	@OPERATION
	void rotate(int degree){
		//TODO implement
	}
	
	@OPERATION
	void color(string col){
		//TODO implement
	}
	
}

