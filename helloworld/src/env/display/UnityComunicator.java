package display;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import cartago.Artifact;
import cartago.OPERATION;

public class UnityComunicator extends Artifact{

	String hostName = "127.0.0.1";
	int portNumber = 5111;
	ServerSocket javaSocket;
	Socket connection;
	BufferedReader inStream;
	PrintWriter outStream;

	
	public void UnityCommunicator(int port){
		this.portNumber=port;
		connect();
	}
	
	@OPERATION
	void writeOnSocket(String str) {

		outStream.println(str);
		outStream.flush();

	}

	@OPERATION
	void readFromSocket(String str) {

		try {
			str=inStream.readLine();
			signal("msgRecived");
		} catch (IOException e) {
			e.printStackTrace();
		}
		str="";
	}

	public boolean connect() {
		{
			try {

				javaSocket = new ServerSocket(portNumber);
				connection = javaSocket.accept();
				outStream = new PrintWriter(connection.getOutputStream());
				inStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

			return true;
		}

	}
}
