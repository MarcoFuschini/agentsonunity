﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerControllerScript : MonoBehaviour {

    private float xVel;
    private float zVel;
    public float absoluteVel=1;

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        xVel = Input.GetAxis("Horizontal");
        zVel = Input.GetAxis("Vertical");
        Move(new Vector3(xVel, 0.0f, zVel));

        
    }

    void Move(Vector3 move)
    {
        if (rb != null)
        {
            rb.AddForce(move * absoluteVel);
        }else
        {
            this.transform.Translate(move * absoluteVel);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Coins")
        {
            Destroy(col.gameObject);
        }

        if(col.gameObject.tag == "PowerUp")
        {
            Destroy(col.gameObject);
            this.GetComponent<Renderer>().material.color = Color.black;
        }
    }
}
