﻿using UnityEngine;
using System.Collections;

public class CoinBehaviourScript : MonoBehaviour {

    public Vector3 rotationSpeed = new Vector3();


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        this.transform.Rotate(rotationSpeed, Space.World);
    }
}
