﻿using UnityEngine;
using System.Collections;

public class SpawnerScript : MonoBehaviour {

    public GameObject spawnElement;


	// Use this for initialization
	void Start () {
        Spawn();
	}
	


	// Update is called once per frame
	void Update () {
	
	}

    private void Spawn()
    {
        Vector3 pos = this.transform.position;
        Quaternion rot = this.transform.rotation;
        Instantiate(spawnElement,pos,rot);
        
    }
}
