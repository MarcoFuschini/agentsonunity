﻿

public class Message{
    public static char DELIMITER = '|';

    private string raw;

    private string bodyName;
    private string signature;
    private int paramNum = 0;
    private string[] param = new string[0];

    public Message(string raw)
    {
        setRaw(raw);
    }

    public Message(string bodyName, string signature, string[] param)
    {
        setBodyName(bodyName);
        setSignature(signature);
        setParams(param);
    }

    // *****************GETTERS and SETTERS**********************
    public string getRaw()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(bodyName);
        sb.Append(DELIMITER);
        sb.Append(signature);
        sb.Append(DELIMITER);
        sb.Append(paramNum);
        foreach (string s in param)
        {
            sb.Append(DELIMITER);
            sb.Append(s);
        }
        return sb.ToString();
    }

    public void setRaw(string s)
    {
        raw = s;
        string[] st = s.Split(DELIMITER);
        bodyName = st[0];
        signature = st[1];
        paramNum = System.Int32.Parse(st[2]);
        if (paramNum > 0)
        {
            param = new string[paramNum];
            for (int i = 0; i < paramNum; i++)
            {
                param[i] = st[i + 3];
            }
        }
    }

    public string getBodyName()
    {
        return bodyName;
    }

    public void setBodyName(string bodyName)
    {
        this.bodyName = bodyName;
    }

    public string getSignature()
    {
        return signature;
    }

    public void setSignature(string actionSignature)
    {
        this.signature = actionSignature;
    }

    public string[] getParams()
    {
        return param;
    }

    public void setParams(string[] param)
    {
        if (param != null)
        {
            this.paramNum = param.Length;
            this.param = param;
        }
    }

    public int getParamNum()
    {
        return paramNum;
    }

}
