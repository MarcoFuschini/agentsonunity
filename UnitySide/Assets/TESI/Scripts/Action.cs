﻿using UnityEngine;
using System.Collections;

public class Action{
    public static char DELIMITER='|';
	
	private string raw;

	private string bodyName;
	private string actionSignature;
	private int paramNum=0;
	private string[] param=new string[0];

	public Action() {
		raw = "";
	}

	public Action(string raw) {
        setRaw(raw);
	}

	public Action(string bodyName, string actionSignature, string[] param) {
		setBodyName(bodyName);
		setActionSignature(actionSignature);
		setParams(param);
	}

	// *****************GETTERS and SETTERS**********************
	public string getRaw() {
		System.Text.StringBuilder sb=new System.Text.StringBuilder();
		sb.Append(bodyName);
		sb.Append(DELIMITER);
		sb.Append(actionSignature);
		sb.Append(DELIMITER);
		sb.Append(paramNum);
		foreach(string s in param) {
			sb.Append(DELIMITER);
			sb.Append(s);
		}
		return sb.ToString();
	}

    public void setRaw(string s){
		raw=s;
		string[] st= s.Split(DELIMITER);
		bodyName=st[0];
		actionSignature=st[1];
		paramNum=System.Int32.Parse(st[2]);
		if(paramNum>0){
			param=new string[paramNum];
			for(int i=0;i<paramNum;i++){
				param[i]=st[i+3];
			}
		}
	}

	public string getBodyName() {
		return bodyName;
	}

	public void setBodyName(string bodyName) {
		this.bodyName = bodyName;
	}

	public string getActionSignature() {
		return actionSignature;
	}

	public void setActionSignature(string actionSignature) {
		this.actionSignature = actionSignature;
	}

	public string[] getParams() {
		return param;
	}

	public void setParams(string[] param) {
		if (param != null) {
			this.paramNum = param.Length;
			this.param = param;
		}
	}

	public int getParamNum() {
		return paramNum;
	}
	
}
