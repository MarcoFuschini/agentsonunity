﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;

public class SocketComunicator : MonoBehaviour {

    private const int BUFF_SIZE=1024;

    public string address = "127.0.0.1";
    public int port = 40404;

    System.Net.Sockets.TcpClient client;
    System.Net.Sockets.NetworkStream sockStream;

    private Queue<string> msgsIN = new Queue<string>();
    private Queue<string> msgsOUT = new Queue<string>();
    private Thread sockReader;
    private bool threadAlive=true;

    void Start () {
        client = new System.Net.Sockets.TcpClient();
        client.Connect(address, port);
        sockStream = client.GetStream();
        sockReader = new Thread(this.socketInspector);
        sockReader.Start();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //TODO DA TOGLIERE
        //Thread.Sleep(50);
        //per ora al massimo legge 1 msg e scrive 1 msg per frame
       /* if (sockStream.DataAvailable)
        {
            log("socket aggiungo messaggio");
            msgsIN.Enqueue(sockRead());
            sockStream.Flush();
        }*/
        if (msgsOUT.Count > 0)
        {
            int size = msgsOUT.Count;
            //invia tutti i messaggi in attesa fino a quel momento
            //log("invio " + size + " messaggi");
            for (int i = 0; i < size; i++)
            {
                sockWrite(msgsOUT.Dequeue());
            }
        }
        if (!client.Connected)
        {
            log("Spegnimento");
            //Application Quit funziona solo sulla build, non sugli avvii di test
            Application.Quit();
        }
	}

    void OnDestroy()
    {
        sockStream.Close();
        client.Close();
        threadAlive = false;
        sockReader.Join();
    }


    //un thread separato gira
    private void socketInspector()
    {
        while (threadAlive)
        {
            if (sockStream.DataAvailable)
            {
                //Debug.Log("[readSock] sto leggendo");
                //log("socket aggiungo messaggio");
                msgsIN.Enqueue(sockRead());
                sockStream.Flush();
            }
        }
    }

    //queste due funzioni sock... scrivono e leggono direttamente dalla socket
    private string sockRead()
    {
        byte[] inStream = new byte[BUFF_SIZE];
        sockStream.Read(inStream, 0,inStream.Length);
        return System.Text.Encoding.ASCII.GetString(inStream);
    }
    private void sockWrite(string msg)
    {
        string msgToSend = msg+'\r';

        byte[] outStream = System.Text.Encoding.ASCII.GetBytes(msgToSend.ToCharArray());

        //log("invio: " + msgToSend);

        sockStream.Write(outStream, 0, outStream.Length);
        sockStream.Flush();
    }

    //queste due funzioni vengono chiamate dall'esterno per leggere e scrivere i messaggi a jacamo
    public string readMessage()
    {
        if (msgsIN.Count > 0)
        {
            return msgsIN.Dequeue();
        }
        else
        {
            return "";
        }
    }

    public void sendMessage(string msg)
    {
        msgsOUT.Enqueue(msg);
    }

    public bool messagePresent()
    {
        return msgsIN.Count > 0;
    }

    public int messagesInEnqueued()
    {
        return msgsIN.Count;
    }


    private void log(string s)
    {
        Debug.Log("[" + this.name + "]" + s);
    }
}
