﻿using UnityEngine;
using System.Collections;

public class spola : MonoBehaviour {

    public float vel = 1;
    bool avanti = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (avanti)
        {
            transform.Translate(new Vector3(vel, 0f, 0f));
        }
        else
        {
            transform.Translate(new Vector3(-vel, 0f, 0f));
        }

        if (transform.position.x > 3.5)
        {
            avanti = false;
        }
        if(transform.position.x < -3.5)
        {
            avanti = true;
        }
	}

}
