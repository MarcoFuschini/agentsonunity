﻿using UnityEngine;
using System.Collections;
using System;

public class cubeBodyScript : VirtualBody {

    void OnTriggerEnter(Collider c)
    {
        perceive(new Message(this.name,"collision",new string[] { }));
    }

    public override void doAction(Message msg)
    {
        if ("rotate".Equals(msg.getSignature()))
        {
            this.transform.Rotate(
                new Vector3(System.Int32.Parse(msg.getParams()[0]),
                            System.Int32.Parse(msg.getParams()[1]),
                            System.Int32.Parse(msg.getParams()[2])));
        }

        if ("translate".Equals(msg.getSignature()))
        {
            this.transform.Translate(
                new Vector3(System.Int32.Parse(msg.getParams()[0]),
                            System.Int32.Parse(msg.getParams()[1]),
                            System.Int32.Parse(msg.getParams()[2])));
        }

        if ("changeColor".Equals(msg.getSignature()))
        {
            System.Random rand = new System.Random();
            float r = 0, g=0, b=0;
            r=(float)rand.NextDouble();
            g = (float)rand.NextDouble();
            b = (float)rand.NextDouble();

            gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b);
        }
    }
}
