﻿using UnityEngine;
using System.Collections;

public abstract class VirtualBody : MonoBehaviour {

    public GameObject comunicatore;
    private Switch sw;
	// Use this for initialization
	void Start () {
        sw = (Switch)comunicatore.GetComponent(typeof(Switch));
        sw.registerBody(this.name);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected void log(string s)
    {
        Debug.Log("[" + this.name + "]"+s);
    }

    public void perceive(Message p)
    {
        sw.trasmitPerception(p);
    }

    public abstract void doAction(Message action);
}
