﻿using UnityEngine;
using System.Collections;
using System;

public class Switch : MonoBehaviour {

    private SocketComunicator socket;
    private ArrayList registred = new ArrayList();

	// Use this for initialization
	void Start () {
        socket=(SocketComunicator)this.GetComponent("SocketComunicator");
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        int msgCount = socket.messagesInEnqueued();
        //log("Ricevuti " + msgCount + " messaggi");
        if (socket.messagePresent())
        {
            for(int i = 0; i < msgCount; i++)
            {
                Message a = new Message(socket.readMessage());
                deliverAction(a);
            }
            
        }
	}

    internal void trasmitPerception(Message perception)
    {
        socket.sendMessage(perception.getRaw());
    }

    private void deliverAction(Message a)
    {
        //Sarebbe più rapido usando un hashmap al posto dell'arrayList 
        //come registred e ricercare i valori per chiave?

       //log("ricevuto: "+a.getRaw());
        foreach (GameObject g in registred)
        {
            if (g.name.Equals(a.getBodyName()))
            {
                ((VirtualBody)g.GetComponent(typeof(VirtualBody))).doAction(a);
            }
        }
    }

    public void registerBody(string bodyName)
    {
        registred.Add(GameObject.Find(bodyName));
    }

    private void log(string s)
    {
        Debug.Log("[" + this.name + "]" + s);
    }

    public void unregisterBody(string bodyName)
    {
        registred.Remove(GameObject.Find(bodyName));
    }
}
