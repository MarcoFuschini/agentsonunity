// CArtAgO artifact code for project collaborativeAgentsTest1

package collaborativeAgentsTest1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import cartago.*;

public class TcpServerArtifact extends Artifact {
	
	ServerSocket serverSocket;
	Socket connection;
	int port=0;
	
	PrintWriter out;
	BufferedReader in;
	Thread controller;
	private boolean active=true;
	
	
	void init(int initialValue) {
		
		try {
			serverSocket=new ServerSocket();
			port=serverSocket.getLocalPort();
			defineObsProperty("localPort", port);
			final TcpServerArtifact server=this;
			signal("debugConsole");
			controller=new Thread(new Runnable() {
				public void run() {
					try {
						while(active){
							System.out.println("Controller LOOP");
							server.in.ready();
							signal("serverReady");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	void connect(){
		try {
			connection=serverSocket.accept();
			System.out.println("SERVER COLLEGATO");
			out=new PrintWriter(connection.getOutputStream());
			in= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			controller.start();
			defineObsProperty("serverConnection", connection);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	void write(String s){
		out.print(s);
		out.flush();
	}
	
	@OPERATION
	void read(OpFeedbackParam<String> s){
		try {
			s.set(in.readLine());
		} catch (IOException e) {
			e.printStackTrace();
			s.set("ERROR");
		}
	}
	
	@OPERATION
	void stopServer(){
		active=false;
	}
	
}

