// CArtAgO artifact code for project collaborativeAgentsTest1

package collaborativeAgentsTest1;

import cartago.*;

public class Cucina extends Artifact {
	
	@OPERATION
	void init(int initialValue) {
		defineObsProperty("count", initialValue);
		System.out.println("INIT CUCINA");
	}
	
	@OPERATION
	void sforna(OpFeedbackParam<Integer> cibo){
		Integer uscita=new Integer((int) Math.round(Math.random()*100));
		System.out.println("SFORNO");
		cibo.set(uscita);
	}
}

