// Agent cameriere in project collaborativeAgentsTest1

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */


+!start <- .print("sono un cameriere");.my_name(Name); out("cameriere",Name); makeArtifact("c0","collaborativeAgentsTest1.Cucina",[1],C); +forno(C).
+buyfood(Name,Level) : true <- 
	.print("Ricevuto richiesta da ",Name," per ",Level," cibo.");
	!inviaCibo(Level,Name).
	
+!inviaCibo(Level,Name): available(Val) <- .print("invio ",Val," cibo a ",Name);.send(Name,tell,sellfood(Val)); -available(Val).
//+!inviaCibo(Level,Name): not available(Val) <- .random(BaseRandom); +available(math.round(BaseRandom*50)); !inviaCibo(Level,Name).
+!inviaCibo(Level,Name): not available(Val) & forno(C) <- sforna(Quantita); +available(Quantita); !inviaCibo(Level,Name).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
//{ include("$jacamoJar/templates/org-obedient.asl") }
