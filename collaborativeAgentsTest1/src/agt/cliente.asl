// Agent cliente in project collaborativeAgentsTest1

/* Initial beliefs and rules */

hungry_limit(0).

/* Initial goals */

!setHungry.

/* Plans */

+!setHungry : true <- .random(RandomBase); +hungryLevel(math.round(RandomBase*100)); in("cameriere",Name); out("cameriere",Name); +cameriere(Name) ;!!hungryLevel(0).
+?limit(L) : hungry_limit(L).


+!hungryLevel(T) : hungryLevel(HL) & HL<=T <- .print("sono a posto").
+!hungryLevel(T) : hungryLevel(HL) & HL>T & cameriere(Came)<- ?myname(Name); .send(Came,tell,buyfood(Name,HL)).

+?myname(Name) <- .my_name(Name).

+sellfood(Val): hungryLevel(HL) <- -+hungryLevel(HL-Val); !hungryLevel(0).


/*
 //sicuro funzionante
+!hungryLevel(T) : hungryLevel(HL) & (HL>T) <- .print(HL); -+hungryLevel(HL-1); !hungryLevel(T).
+!hungryLevel(T) : hungryLevel(HL) & (HL<T) <- .print(HL); -+hungryLevel(HL+1); !hungryLevel(T).
+!hungryLevel(T) : hungryLevel(T) <- .print("sono a posto"). 
-!hungryLevel(T) <- .print("ho fallito").
*/

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
{ include("$jacamoJar/templates/org-obedient.asl") }
