// Agent comunicatore_server in project collaborativeAgentsTest1

/* Initial beliefs and rules */

/* Initial goals */

!setupServer.

/* Plans */

+!setupServer : true <- .print("ciao");makeArtifact("server0","collaborativeAgentsTest1.TcpServerArtifact",[],S); +id(server,S); focus(S).

+serverReady <- read(Msg);.print("Ricevuto: ",Msg).

+!killServer <- stopServer.

+debugConsole <- .print("boh").


{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
//{ include("$jacamoJar/templates/org-obedient.asl") }
