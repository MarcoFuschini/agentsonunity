package agentsOnUnity.Bodies;

import agentsOnUnity.JavaObjects.AgentVirtualBody;
import agentsOnUnity.JavaObjects.Message;
import cartago.OPERATION;

public class SphereBody extends AgentVirtualBody {

	@OPERATION
	void percezione(Message msg){
//		System.out.println("PERCEPITO: "+msg.getSignature());
		if("collision".equals(msg.getSignature())){
			System.out.println("colliso con "+msg.getParams()[0]);
			if("CubeRight".equals(msg.getParams()[0])){
				signal("direzione","sinistra");				
			}
			if("CubeLeft".equals(msg.getParams()[0])){
				signal("direzione","destra");				
			}
		}
		
		if("slowdown".equals(msg.getSignature())){
			signal("accellera");
		}

	}
	
	@OPERATION
	void muoviAvanti(){
		Message msg=new Message(bodyName, "muovi", new String[]{"2","0","0"});
		doAction(msg);
	}
	
	@OPERATION
	void muoviIndietro(){
		Message msg=new Message(bodyName, "muovi", new String[]{"-2","0","0"});
		doAction(msg);
	}
}
