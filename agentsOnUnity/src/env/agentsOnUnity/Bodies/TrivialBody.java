package agentsOnUnity.Bodies;

import agentsOnUnity.JavaObjects.Message;
import agentsOnUnity.JavaObjects.AgentVirtualBody;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

public class TrivialBody extends AgentVirtualBody {

	
	
	@OPERATION
	void ruota(){
		Message msg=new Message(bodyName,"rotate",new String[]{"0","5","0"});
		doAction(msg);
	}
	
	@OPERATION
	void transla(){
		Message msg=new Message(bodyName,"translate",new String[]{"1","0","0"});
		doAction(msg);
	}

	@OPERATION
	void cambiaColore(){
		Message msg=new Message(bodyName,"changeColor",new String[]{});
		doAction(msg);
	}
	
	@OPERATION
	void percezione(Message msg){
//		System.out.println("PERCEPITO: "+msg.getSignature());
		if("collision".equals(msg.getSignature())){
			cambiaColore();
		}
		
		if("slowdown".equals(msg.getSignature())){
			ruota();
		}
	}

}
