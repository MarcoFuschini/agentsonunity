// CArtAgO artifact code for project agentsOnUnity

package agentsOnUnity.Bodies;

import agentsOnUnity.JavaObjects.AgentVirtualBody;
import agentsOnUnity.JavaObjects.Message;
import cartago.*;

public class EchoBody extends AgentVirtualBody {
	
	
	@OPERATION
	public void echo(String toResend){
		System.out.println("[EchoBody] ricevuto questo: "+toResend);
		Message m=new Message(bodyName, toResend, new String[]{});
		doAction(m);
	}
	
	
	@OPERATION
	public void echo(Message m){
		doAction(m);
	}
	
}

