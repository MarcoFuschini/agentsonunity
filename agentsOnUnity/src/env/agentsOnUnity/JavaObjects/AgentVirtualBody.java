package agentsOnUnity.JavaObjects;

import agentsOnUnity.Boundary.Switch;
import cartago.Artifact;
import cartago.ArtifactId;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import jason.stdlib.string;

public class AgentVirtualBody extends Artifact{

	private Switch connectedSwitch;
	//bodyName � il nome del gameObject su Unity
	protected String bodyName;

	@OPERATION
	void init(String bodyName){
		this.bodyName=bodyName;
	}
	
	public void setSwitch(Switch s){
		connectedSwitch=s;
	}
	
	@OPERATION
	public void perceived(Message p){
		this.beginExternalSession();
		signal("perception",p);
		this.endExternalSession(true);
	};
	
	
	public void doAction(Message a){
		signal("faiAzione",a.getRaw());
//		connectedSwitch.doAction(a);
	}
	
	@OPERATION
	public void getBodyName(OpFeedbackParam<String> bodyName){
		bodyName.set(this.bodyName);
	}
	
	//i corpi devono creare un oggetto Action e chiamare doAction(Action a)

}
