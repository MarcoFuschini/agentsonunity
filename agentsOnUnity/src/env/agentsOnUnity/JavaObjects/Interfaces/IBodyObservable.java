package agentsOnUnity.JavaObjects.Interfaces;

import agentsOnUnity.JavaObjects.AgentVirtualBody;

public interface IBodyObservable {

	public void addListener(AgentVirtualBody listener);
	public void removeListener(AgentVirtualBody listener);
	
	
}
