package agentsOnUnity.JavaObjects;

import java.util.StringTokenizer;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

public class Message {

	public static final String DELIMITER = "|";

	private String raw;

	private String bodyName;
	private String signature;
	private int paramNum = 0;
	private String[] params = new String[0];

	public Message() {
		raw = "";
	}

	public Message(String raw) {
		setRaw(raw);
	}

	public Message(String bodyName, String signature, String[] params) {
		setBodyName(bodyName);
		setSignature(signature);
		setParams(params);
	}

	// *****************GETTERS and SETTERS**********************
	public String getRaw() {
		StringBuilder sb = new StringBuilder();
		sb.append(bodyName);
		sb.append(DELIMITER);
		sb.append(signature);
		sb.append(DELIMITER);
		sb.append(paramNum);
		for (String s : params) {
			sb.append(DELIMITER);
			sb.append(s);
		}
		return sb.toString();
	}

	public void setRaw(String s) {
		try {
			raw = s;
			StringTokenizer st = new StringTokenizer(s, DELIMITER);
			bodyName = st.nextToken();
			signature = st.nextToken();
			paramNum = Integer.parseInt(st.nextToken());
			if (paramNum > 0) {
				params = new String[paramNum];
				for (int i = 0; i < paramNum; i++) {
					params[i] = st.nextToken();
				}
			}
		} catch (Exception e) {
			System.out.println("[Message] eccezione: "+e);
			bodyName="";
			signature="";
			paramNum=0;
			params=new String[]{};
		}
	}

	public String getBodyName() {
		return bodyName;
	}

	public void setBodyName(String bodyName) {
		this.bodyName = bodyName;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		if (params != null) {
			this.paramNum = params.length;
			this.params = params;
		}
	}

	public int getParamNum() {
		return paramNum;
	}
	// **********************************************************
}
