package agentsOnUnity.Boundary;

import java.util.ArrayList;
import java.util.HashMap;

import agentsOnUnity.JavaObjects.Message;
import agentsOnUnity.JavaObjects.AgentVirtualBody;
import cartago.Artifact;
import cartago.ArtifactId;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

public class Switch extends Artifact{

	HashMap<String, ArtifactId> bodiesMap=new HashMap<String, ArtifactId>();
	HashMap<String, ArtifactId> mindMaps=new HashMap<String, ArtifactId>();
	
	HashMap<String, String> bodyToMind=new HashMap<String, String>();

	@OPERATION
	void registraBody(String nomeMente,String nomeCorpo,ArtifactId avb){
		
		bodiesMap.put(nomeCorpo, avb);
		mindMaps.put(nomeMente, avb);
		bodyToMind.put(nomeCorpo, nomeMente);
		
		
		
		System.out.println("Registro: "+avb+" per "+nomeMente+">"+nomeCorpo);
//		avb.setSwitch(this);
	}
	
	@OPERATION
	void receivedPerception(String s,OpFeedbackParam<Message> msg){
		System.out.println("[Switch] ricevuta: "+s);
		
		//Manda direttamente la percezione all'agente, senza passare dal VirtualBody
		Message perc=new Message(s);
		msg.set(perc);
		//TODO fare in modo di passare la percezione direttamente al VirtualBody
		
	}
	
	@OPERATION
	void getBodyFromPerception(Message msg,OpFeedbackParam<String> bodyName){
		bodyName.set(msg.getBodyName());
	}
	
	@OPERATION
	void getMindFromBody(String bodyName,OpFeedbackParam<String> mindId){
		String mindName=bodyToMind.get(bodyName);
		if(mindName==null) mindName="";
		mindId.set(mindName);
	}
	
	@OPERATION
	void bodyArtifactFromMind(String mindName,OpFeedbackParam<ArtifactId> bodyArt){
		ArtifactId ba=mindMaps.get(mindName);
		//System.out.println("[Switch] bodyArtID: "+ba);
		bodyArt.set(ba);
	}
	
	@OPERATION
	void sendPerception(AgentVirtualBody body,Message m){
		body.perceived(m);
	}
	
	public void doAction(Message a){
		this.beginExternalSession();
		signal("action",a);
		this.endExternalSession(true);
	}


}
