// CArtAgO artifact code for project collaborativeAgentsTest1

package agentsOnUnity.Boundary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

import cartago.Artifact;
import cartago.OPERATION;
import cartago.ObsProperty;
import cartago.OpFeedbackParam;

public class TcpServerArtifact extends Artifact{
	
	ServerSocket serverSocket;
	Socket connection;
	int port=0;//observable (localPort)
	String myOwner;
	
	LinkedBlockingQueue<String> inBuff = new LinkedBlockingQueue<String>();
	LinkedBlockingQueue<String> outBuff = new LinkedBlockingQueue<String>();
	
	PrintWriter out;
	BufferedReader in;
	Thread readerThread;
	Thread writerThread;
	private boolean threadsActive=true;
	
	@OPERATION
	void init() {
		
		final TcpServerArtifact parent=this;
		defineObsProperty("localPort", port);
		
		System.out.println("[ServerArtifact] ENTRO INIT");
		
		
		readerThread=new Thread(new Runnable() {
			public void run() {
				try {
					
					//attende una connessione
					connection=serverSocket.accept();
					System.out.println("[ServerArtifact] SERVER COLLEGATO");
					
					parent.beginExternalSession();
					signal("connesso");
					parent.endExternalSession(true);
					
					ObsProperty obs= parent.getObsProperty("localPort");
					obs.updateValue(connection.getPort());
					//bindo gli stream in/out per lettura/scrittura
					out=new PrintWriter(connection.getOutputStream());
					in= new BufferedReader(new InputStreamReader(connection.getInputStream()));
					defineObsProperty("serverConnection", connection);
					
					//avviare questo thread solo dopo connessione avvenuta
					writerThread.start();
					//loop "infinito" per ricezioni messaggi
					while(threadsActive){
						System.out.println("[ServerArtifact] reader LOOP");
						in.ready();
						String msg = in.readLine();
						inBuff.offer(msg);
						
						parent.beginExternalSession();
						signal("serverIncomingMsg");
						parent.endExternalSession(true);
					}
					connection.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		writerThread=new Thread(new Runnable() {
			//avviare questo thread DOPO aver effettuato la connessione
			public void run() {
				while(threadsActive){
					try {
						//System.out.println("[ServerArtifact] writer LOOP");
						String msg = outBuff.take();
						//System.out.println("[ServerArtifact] sto inviando: "+msg);
						out.println(msg);
						out.flush();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
	}
	
	@OPERATION
	void connect(){
		signal("scrivi","connettendo");
		
		try {
			serverSocket=new ServerSocket(40404);//mettere a 0 per una porta casuale che poi viene scritta in port e "localPort"
			port=serverSocket.getLocalPort();
			defineObsProperty("localPort", port);
			
			//avvio l'autonomia del server
			readerThread.start();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	void write(String s){
		try {
			outBuff.put(s);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	void read(OpFeedbackParam<String> s){
		s.set(inBuff.poll());
	}
	
	@OPERATION
	void stopServer(){
		threadsActive=false;
	}
	
	@OPERATION
	void setAsOwner(String id){
		myOwner=id;
		System.out.println("[ServerArtifact] il proprietario � "+id);
	}

	
}

