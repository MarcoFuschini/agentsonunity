// Agent comunicatore in project agentsOnUnity

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */


+!start : true <- makeArtifact("server","agentsOnUnity.Boundary.TcpServerArtifact",[],S); setAsOwner("io") ; focus(S); +server(S) ; connect; ?info; !register.
+!register <- .my_name(Name); out("comunicator",Name); in("sorter",Sorter); out("sorter",Sorter) ; +smistatore(Sorter).print("ci siamo conosciuti").

+connesso <- .broadcast(tell,connesso).

+serverIncomingMsg : smistatore(Sorter) <- -serverIncomingMsg;read(Str); .print("Ricevuto: ",Str); .send(Sorter,achieve,ricevuto(Str)).
+!serverOutcomingMsg(Msg): true <- /*.print("provo ad inviare:",Msg);*/ write(Msg).

+?info : localPort(P)<- .print("porta: ",P).
+?info : true <- .print("non trovo la porta").

+scrivi(Msg): true <- .print(Msg).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
{ include("$jacamoJar/templates/org-obedient.asl") }
