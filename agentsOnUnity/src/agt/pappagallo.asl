
!start.

{ include("connettore.asl")}

+!start <- !creaCorpo("agentsOnUnity.Bodies.EchoBody","EchoBody").

+!corpoPronto <- .print("Sono pronto, attendo una percezione per rispedirla").

+perception(Msg) <- -perception(Msg);.print("ricevuto percezione: ",Msg); echo(Msg).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
