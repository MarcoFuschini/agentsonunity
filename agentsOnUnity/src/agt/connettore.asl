
//fa in modo di collegare tutto in sordina
//bisogna chiamare il goal !creaCorpo(BodyClass,BodyName) con BodyClass come artefatto esteso da AgentVirtualBody e BodyName il nome del GameObject di Unity

//usare il trigger +perception(Msg) per ricevere le percezioni del corpo, Msg � di classe Message
+!creaCorpo(BodyClass,BodyName)<-
						.my_name(MindName);
						+bodyName(BodyName);
						+mindName(MindName);
						.concat(MindName,"_",BodyName,ArtifactName);
						makeArtifact(ArtifactName,BodyClass,[],B);
						focus(B);
						init(BodyName);
						+mybody(B);
						.print("corpo creato:",ArtifactName).

+connesso : mybody(B) & bodyName(BodyName) & mindName(MindName) <-
					in("sorter",Sorter);
					out("sorter",Sorter);
					+sorter(Sorter);
					.send(Sorter,tell,nuovoCorpo(MindName,BodyName)).
+connesso : true <- -connesso;.wait(50);+connesso.
