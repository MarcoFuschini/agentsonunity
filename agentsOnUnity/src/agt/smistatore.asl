// Agent smistatore in project agentsOnUnity

/* Initial beliefs and rules */

/* Initial goals */


/* Plans */

+connesso : true <- makeArtifact("switch","agentsOnUnity.Boundary.Switch",[],S);focus(S);+switch(S);.my_name(Name); out("sorter",Name); in("comunicator",Com) ;+comunicatore(Com); .print("ci siamo conosciuti").

+!ricevuto(Str) : comunicatore(C) <- .print("Ricevuto: ",Str);receivedPerception(Str,Msg);getBodyFromPerception(Msg,BodyName);!!checkBodyName(BodyName,Msg).
+!inviato(Msg) : comunicatore(C) <-  -inviato(Msg); /*.print("passo: ",Msg);*/ .send(C,achieve,serverOutcomingMsg(Msg)).

+!checkBodyName(BodyName,Msg): BodyName=="" <-.print("Percezione invalida").
+!checkBodyName(BodyName,Msg): true <- getMindFromBody(BodyName,MindName);!!checkMindName(MindName,Msg).

+!checkMindName(MindName,Msg): MindName=="" <-.print("Nome del corpo non valido").
//+!checkMindName(MindName,Msg): true <- .send(MindName,tell,perception(Msg)).
+!checkMindName(MindName,Msg): true <- bodyArtifactFromMind(MindName,BodyArt); perceived(Msg)[artifact_id(BodyArt)].

+nuovoCorpo(MindName,BodyName) : switch(S)&comunicatore(C) <- .print("ricevuto nuovo corpo da registrare: ",BodyName," per ",MindName);.concat(MindName,"_",BodyName,ArtifactName);lookupArtifact(ArtifactName,Body);focus(Body);registraBody(MindName,BodyName,Body)[artifact_id(S)];.send(MindName,achieve,corpoPronto).
+nuovoCorpo(MindName,BodyName) : true <- .print("riprovo tra poco");.wait(10);-nuovoCorpo(MindName);+nuovoCorpo(MindName).

+faiAzione(A): true <- /*.print("azione da inviare: ",A);*/-faiAzione(A);!inviato(A).


{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
//{ include("$jacamoJar/templates/org-obedient.asl") }
