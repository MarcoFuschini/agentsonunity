
!start.

{ include("connettore.asl")}

+!start <- !creaCorpo("agentsOnUnity.Bodies.SphereBody","Sphere").

+!corpoPronto <- .print("faccio qualcosa");!lavora.

+!lavora <- !muovi.

+!muovi : direzione(destra) <- muoviAvanti.
+!muovi : direzione(sinistra) <- muoviIndietro.
+!muovi : true <- muoviAvanti.

+direzione(destra) <- -direzione(sinistra).
+direzione(sinistra) <- -direzione(destra).

+accellera <- +accellera; !muovi.

+perception(Msg) <- -perception(Msg);.print("ricevuto percezione",Msg); percezione(Msg).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have a agent that always complies with its organization  
//{ include("$jacamoJar/templates/org-obedient.asl") }
